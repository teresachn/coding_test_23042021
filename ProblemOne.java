import java.io.*;
import java.util.*;

public class ProblemOne {

  public static long[] substractNotZero(long[] a) {
    /*
      Return all numbers (in a form of array of integers) that when
      subtracted by any of integers in that array, doesn't return
      number that is < 0.
      
      Most simple solution is to return number with highest value, since
      if its substracted with every element, its results would never be
      less than 0
      If there is more than one element with max value, count them to be
      repeated in result array
    */

    // Find max value
    /* 
      The complexity of this algorithm at most is 2n - 1 (O(n)), since
      each element must be compared to maxVal at most twice
      (once for first element).
    */
    long maxVal = Long.MIN_VALUE;
    int countMax = 0;
    for (int i = 0; i < a.length; i++) {
      if (maxVal < a[i]) {
        maxVal = a[i];
        countMax = 1;
      } else if (maxVal == a[i]) {
        countMax++;
      }
    }

    long[] resultArr = new long[countMax];
    for (int i = 0; i < resultArr.length; i++) {
      resultArr[i] = maxVal;
    }
    return resultArr;
  }

    public static void main(String[] args) throws IOException {
        // Receive input
        Scanner scanner = new Scanner(System.in);

        // Assumption input was formatted like this
        // 3           Number of integer to be inputted in array
        // 4 2 3       Array elements
        int n = scanner.nextInt();

        long[] arr = new long[n];
        for (int i = 0; i < n; i++) {
          arr[i] = scanner.nextLong();
        }

        long[] result = substractNotZero(arr);

        // Print result array
        for (int i = 0; i < result.length; i++) {
          System.out.print(result[i]);
          if (i < result.length - 1) {
            System.out.print(" ");
          } else {
            System.out.println();
          }
        }

        scanner.close();
    }
}
