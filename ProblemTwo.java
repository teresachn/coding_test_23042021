import java.io.*;
import java.util.*;

public class ProblemTwo {

  public static long[] divideNotReturnX(long[] arr, long x) {
    /*
        Return all numbers (in a form of array of integers) that when
        divided by any of integers in nums doesn't return x.
        
        A way to solve this is to iterate nums and try to divide with
        nums element, and only put them into result array if all divided
        is not x.
        Because number of elements satifying the condition is unknown,
        use list to record element dynamically, then convert list to array
    */

    /*
      The best time complexity of this algorithm is n if all element in
      this array is not multiply of x (O(n)).
      The worst time complexity is n^2 to search factor of current element
      other than x for each element in array (O(n^2)).
    */
    List<Long> resultList = new ArrayList<Long>();
    for (int i = 0; i < arr.length; i++) {
      long currentElement = arr[i];
      if (arr[i] % x != 0) {
        // Add to list if not divisible by x
        resultList.add(currentElement);
        continue;
      }

      // If divisible then search other factor a that a*x == current element

      boolean resultX = false;
      int j = 0;
      long a = arr[i] / x;
      while (!resultX && j < arr.length) {
        resultX = arr[j] == a;
        j++;
      }
      if (!resultX) {
        resultList.add(currentElement);
      }
    }

    /*
      Convert list to stream of Integer, then map its values to int.
      The sequence of mapped values then converted to array.
    */
    long[] result = resultList.stream().mapToLong(Long::longValue).toArray();
    return result;
  }

  public static void main(String[] args) throws IOException {
    // Receive input
    Scanner scanner = new Scanner(System.in);

    // Assumption input was formatted like this
    // 5 4             Number of integer to be inputted in array and x value
    // 1 2 3 4 5       Array elements
    int n = scanner.nextInt();
    long x = scanner.nextLong();

    long[] arr = new long[n];
    for (int i = 0; i < n; i++) {
      arr[i] = scanner.nextLong();
    }

    long[] result = divideNotReturnX(arr, x);

    // Print result array
    for (int i = 0; i < result.length; i++) {
      System.out.print(result[i]);
      if (i < result.length - 1) {
        System.out.print(" ");
      } else {
        System.out.println();
      }
    }

    scanner.close();
  }
}
