import java.io.*;
import java.util.*;

public class ProblemThree {

  public static String[] wordWithLengthX(String[] word, int x) {
    /*
      Return an array of strings containing all strings that has length x
      
      The solution is to iterate word array and check if length of the word
      element is x. Because the number of word with length x is unknown, it
      is suggested to use list to add word in dynamic array.
    */

    /*
      The complexity of this algorithm is n (O(n)) because it has to traverse
      all element of array to find out if length of each word equal to x.
    */
    List<String> resultArr = new ArrayList<String>();
    for (int i =  0; i < word.length; i++) {
      if (word[i].length() == x) {
        resultArr.add(word[i]);
      }
    }

    String[] result = new String[resultArr.size()];
    resultArr.toArray(result);
    return result;
  }

  public static void main(String[] args) throws IOException {
    // Receive input
    Scanner scanner = new Scanner(System.in);

    /* Assumption input was formatted like this
       4                      x value as valid word length
       I am you young bold    Array elements separated by space
    */
    // int n = scanner.nextInt();
    int x = scanner.nextInt();

    // Skip extra enter after reading array length and word length value
    scanner.nextLine();

    // Read input in one line and split by space
    String[] word = scanner.nextLine().split(" ");

    String[] result = wordWithLengthX(word, x);

    // Print result array
    for (int i = 0; i < result.length; i++) {
      System.out.print(result[i]);
      if (i < result.length - 1) {
        System.out.print(" ");
      } else {
        System.out.println();
      }
    }

    scanner.close();
  }
}
